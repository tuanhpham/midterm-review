/**
 * 
 */

/**
 * @author bricks
 *
 */
public class Main {

	static IAnimal cat = new Cat();
	static IAnimal goat = new Goat();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		System.out.println("Welcome to the farm!!! It's smells great!");
		System.out.println("Let's feed some animals some food and see what happens.");
		
		checkHappiness();
		
		feedCatFood();
		
		checkHappiness();
		
		foodVegetableLeftOvers();
		
		checkHappiness();
		

	}

	private static void foodVegetableLeftOvers() {
		System.out.println();
		System.out.println("Okay, let's feed our animals some what we'd normally put down the disposal.");
		cat.feed(new VegetableLeftOvers());
		goat.feed(new VegetableLeftOvers());
		
	}

	private static void feedCatFood() {
		
		System.out.println();
		System.out.println("Okay, let's feed our animals some cat food.");
		cat.feed(new CatFood());
		goat.feed(new CatFood());
		
	}

	private static void checkHappiness() {
		System.out.println();
		System.out.println("Let's see how happy our animals are:");
		System.out.println(cat);
		System.out.println(goat);
		
	}

}
